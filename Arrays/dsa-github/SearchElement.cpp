#include <iostream>
using namespace std;
// function to search for element in array
// arr -> given array
// n -> length of array
// x -> search element
int search(int arr[], int n, int x)
{
    for (int i = 0; i < n; i++)
    {
        if (arr[i] == x)
        {
            return i;
        }
    }
    return -1;
}

int main()
{
    int arr[] = {20, 5, 7, 25}, x = 25;
    cout << "Searched Index = " << search(arr, 4, x) << endl;
}