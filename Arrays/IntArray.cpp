#include <iostream>

class IndexOutOfBoundsException
{
};
class IntArray
{
private:
    int *m_ptr{nullptr};
    int m_size{0};

public:
    IntArray() = default;

    explicit IntArray(int size)
    {
        if (size != 0)
        {
            m_ptr = new int[size]{}; // allocate heap
            m_size = size;
        }
    }

    ~IntArray()
    {
        delete[] m_ptr; // release memory
    }

    bool IsValidIndex(int index)
    {
        return (index >= 0) && (index < m_size);
    }

    int Size() const
    {
        return m_size;
    }

    bool IsEmpty() const
    {
        return (m_size == 0);
    }

    int &operator[](int index)
    {
        if (!IsValidIndex(index))
        {
            throw IndexOutOfBoundsException{};
        }
        return m_ptr[index];
    }
};

int main()
{
    using std::cin;
    using std::cout;

    cout << "Creating an Empty array. \n";
    IntArray a{};
    cout << "Size of this array is: " << a.Size() << ".\n";

    cout << "Creating an array with size 10 \n";
    IntArray b{10};
    cout << "Size of this array is " << b.Size() << ".\n";

    cout << "Setting value 10 to b[0] \n";
    b[0] = 10;
    cout << "Value of b[0] is " << b[0] << "\n";

    for (int i = 1; i <= 10; i++)
    {
        b[i - 1] = i;
    }

    cout << "Array is :";
    for (int i = 0; i < b.Size(); i++)
    {
        cout << b[i] << " ";
    }
    cout << "\nArray size is " << b.Size() << "\n";
    cout << "Enter index to access: ";
    int index{};
    cin >> index;

    try
    {
        cout << "The element at index " << index << " is " << b[index];
    }
    catch (const IndexOutOfBoundsException &)
    {
        cout << "***** IndexOutOfBound: Enter a valid index \n";
    }
}