/*
1. This LinkedList is implemented im C style using the structs.
2. There are no classes in C, and structs and classes are the very same thing in C++. 
3. The only difference is in the default access modifier, which is public for structs, and private for classes.
4. Structs are stored on the stack and classes are stored on the heap. 
5. There can be a performance improvement using a struct for a small amount of information that needs to be passed between objects, but structs lack any class-related capabilities like inheritance and encapsulation.

*/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

struct Node
{
    int data;
    struct Node *next;
} *first = NULL;

// function to create a Linked List
void create(int A[], int n)
{
    struct Node *newNode, *last;                        // create new node pointer and last pointer
    first = (struct Node *)malloc(sizeof(struct Node)); // create first Node and allocate memory to it in Heap
    first->data = A[0];
    first->next = NULL;
    last = first;

    for (int i = 1; i < n; i++)
    {
        newNode = (struct Node *)malloc(sizeof(struct Node));
        newNode->data = A[i];
        newNode->next = NULL;
        last->next = newNode;
        last = newNode;
    }
}

// Function to display Linked List
// to display the linked list from
// we have to pass the pointer from where we want to print the LinkedList
void display(struct Node *p)
{
    printf("%s", "The created Linked List is: \n");
    while (p != NULL)
    {
        printf("%d ", p->data);
        p = p->next;
    }
    printf("\n");
}

int length(struct Node *p)
{
    int count = 0;
    while (p != NULL)
    {
        count++;
        p = p->next;
    }
    return count;
}

int sum(struct Node *p)
{
    int sum = 0;
    while (p != NULL)
    {
        sum += p->data;
        p = p->next;
    }
    return sum;
}

int max(struct Node *p)
{
    int m = INT32_MIN;
    while (p != NULL)
    {
        if (p->data > m)
        {
            m = p->data;
        }
        p = p->next;
    }
    return m;
}

int min(struct Node *p)
{
    int m = p->data;
    while (p != NULL)
    {
        if (p->data < m)
        {
            m = p->data;
        }
        p = p->next;
    }
    return m;
}

int main()
{
    int A[] = {3, 5, 7, 10, 15};
    int len = sizeof(A) / sizeof(A[0]);
    create(A, len);
    display(first);
    printf("Length of Linked List is : %d \n", length(first));
    printf("Sum of Linked List is : %d \n", sum(first));
    printf("Max element in the LinkedList is: %d \n", max(first));
    printf("Min element in the LinkedList is: %d \n", min(first));
    return 0;
}
