#include <iostream>

// Create Node
struct Node
{
    int data;
    struct Node *next;
} *first = NULL;

// take pointer from the Node which you want to diplay
void display(struct Node *p)
{
    while (p != NULL)
    {
        printf("%d", p->data);
        p = p->next;
    }
}