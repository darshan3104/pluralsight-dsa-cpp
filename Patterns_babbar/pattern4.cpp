#include <iostream>
using namespace std;

/*
Print the below pattern
A A A
B B B
C C C

n = 3

*/

int main()
{
    int n = 3;
    int i = 1;
    char ch = 'A';
    while(i <= n){
        int j = 0;
        while(j < n){
            cout << ch;
            j++;
        } 
        cout << endl;
        ch = 'A' + i;
        i++;
    }
    return 0;
}