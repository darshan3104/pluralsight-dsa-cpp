#include <iostream>
using namespace std;

/*
Print the below pattern
1
2 1
3 2 1
4 3 2 1

n = 4

*/
int main()
{

    int n = 4;
    int i = 1;
    while (i <= n)
    {
        int j = 1;
        while (j <= i)
        {
            cout << i - j + 1 << " ";
            j++;
        }
        cout << endl;
        i++;
    }

    return 0;
}