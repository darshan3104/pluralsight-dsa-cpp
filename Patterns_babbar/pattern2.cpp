#include <iostream>
using namespace std;
/*
Print the below pattern
1
2 3
3 4 5
4 5 6 7

n = 4

*/

int main()
{

    int n = 4;
    int i = 1;
    while (i <= n)
    {
        int j = 0;
        while (j < i)
        {
            cout << j + i << " ";
            j++;
        }
        cout << endl;
        i++;
    }
}