#include <iostream>
using namespace std;

class Patterns
{
public:
    Patterns(){};

    /*
    Print the below using printTypeOne function
    r=1  A
    r=2  B B
    r=3  C C C

    n = 3

    */
    void printTypeOne(int n)
    {
        int i = 1;
        while (i <= n)
        {

            int j = 1;
            while (j <= i)
            {
                char ch = 'A' + i - 1;
                cout << ch << " ";
                j++;
            }
            cout << endl;
            i++;
        }
    }

    /**
        Print the below using printTypeTwo function
        r=1  A
        r=2  B C
        r=3  D E F

        n = 3

    */
    void printTypeTwo(int n)
    {
        int i = 1;
        char ch = 'A';
        while (i <= n)
        {
            int j = 1;
            while (j <= i)
            {
                cout << ch << " ";
                ch++;
                j++;
            }
            i++;
            cout << endl;
        }
    }

    /**
        Print the below using printTypeThree function
        r=1  A
        r=2  B C
        r=3  C D E
             D E F G 

        n = 3

    */
};

int main()
{
    Patterns obj;
    // obj.printTypeOne(5);
    obj.printTypeTwo(5);
    return 0;
}
