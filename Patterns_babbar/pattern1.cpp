#include <iostream>
using namespace std;
/*
Print the below pattern
1 2 3 4
1 2 3 4
1 2 3 4
1 2 3 4

n = 4

*/

int main()
{

    int n = 4;
    int i = 1;
    while (i <= n)
    {
        int j = 1;
        while (j <= n)
        {
            cout << j << " ";
            j++;
        }
        cout << endl;
        i++;
    }
}