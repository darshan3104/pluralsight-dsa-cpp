#include <iostream>
using namespace std;

/*
Print the below pattern
  r=1  A B C
  r=2  A B C
  r=3  A B C

n = 3

*/


/*
Print the below pattern
  r=1  A B C
  r=2  B C D
  r=3  C D E

n = 3

*/

int main()
{
    int n = 3;
    int i = 1;
    // while (i <= n)
    // {

    //     int j = 0;
    //     while (j < n)
    //     {
    //         char ch = 'A' + j;
    //         cout << ch << " ";
    //         j++;
    //     }
    //     cout << endl;
    //     i++;
    // }

    while (i <= n)
    {
        int j = 1;
        while (j <= n)
        {
            char ch = 'A' + i + j - 2;
            cout << ch << " ";
            j++;
        }
        cout << endl;
        i++;
    }


    return 0;
}