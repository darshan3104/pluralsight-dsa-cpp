class Node
{
private:
    int data;
    class Node *next;

public:
    Node(int x, class Node *nxt)
    {
        data = x;
        next = nxt;
    };

    friend class LinkedList;
};

class LinkedList
{
private:
    class Node *head;
    class Node *tail;
    int length;

public:
    LinkedList()
    {
        head = NULL;
        tail = NULL;
    };

    ~LinkedList()
    {
        class Node *iter = head;
        while (iter != NULL)
        {
            class Node *temp = iter;
            iter = iter->next;
            delete temp;
        }
    }

    // print linkedlist
    void print()
    {
        printf("\n");
        class Node *iter = head;
        while (iter != NULL)
        {
            printf("%d ", iter->data);
            iter = iter->next;
        }
    }

    // addFront
    void addFront(int x)
    {
        if (head == NULL)
        {
            head = new Node(x, NULL);
            tail = head;
        }
        else
        {
            class Node *temp = head;
            head = new Node(x, temp);
        }
        length++;
    };

    // remove front
    void removeFront()
    {
        if (head == NULL)
            return;

        class Node *temp = head;
        head = head->next;
        delete temp;
        length--;
    };

    // is Empty
    bool isEmpty()
    {
        return (length > 0);
    }
};
