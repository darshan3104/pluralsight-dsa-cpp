#include "list.h"

class Stack
{
private:
    class LinkedList ll;

public:
    void push(int x)
    {
        ll.addFront(x);
    };

    void pop()
    {
        ll.removeFront();
    };

    bool isEmpty()
    {
        return ll.isEmpty();
    }

    void print()
    {
        ll.print();
    };
};