#include <stdio.h>
#include <vector>

using namespace std;

class TreeNode
{

private:
    int data;
    TreeNode *parent;
    vector<TreeNode *> children;

public:
    // Constructor
    TreeNode(int value, TreeNode *parentNode = NULL)
    {
        data = value;
        parent = parentNode;
    }

    // Destructor
    ~TreeNode()
    {
        for (auto &child : children)
        {
            delete child;
        }
    }

    // function to add child to the current TreeNode
    void addChild(int childValue)
    {
        TreeNode *childNode = new TreeNode(childValue, this); // 'this' is the parent
        children.push_back(childNode);
    }

    // function to display the TreeNode
    void printNode()
    {
        printf("\nData: %d", data);
        printf("\nChildren: ");
        if (!children.empty())
        {
            for (TreeNode *child : children)
            {
                child->printNode();
            }
        }
        else
        {
            printf("No Children");
        }
    }

    friend class Tree;
};

class Tree
{
private:
    TreeNode *root;

public:
    Tree(int data)
    {
        root = new TreeNode(data);
    }

    // Destructor to delete the tree nodes
    ~Tree()
    {
        delete root;
    }

    // getParent of a child
    TreeNode *getParent(int x)
    {
        if (root->data == x)
        {
            return root->parent;
        }

        for (const auto &child : root->children)
        {
            if (child->data == x)
            {
                return child->parent;
            }
        }

        return NULL;
    }

    // search function
    TreeNode *search(TreeNode *current, int searchData) const
    {
        if (current == nullptr)
        {
            return nullptr;
        }

        if (current->data == searchData)
        {
            return current;
        }

        for (const auto &child : current->children)
        {
            TreeNode *foundNode = search(child, searchData);
            if (foundNode != nullptr)
            {
                return foundNode;
            }
        }

        return nullptr;
    }

    // insert node in tree
    void insertChild(int x, int parent)
    {

        class TreeNode *parentNode = search(root, parent);

        if (parentNode != NULL)
        {
            parentNode->addChild(x);
        }
        else
        {
            printf("\nParent with data %d not found.", x);
        }
    }

    // print tree
    void print()
    {
        root->printNode();
    }
};

int main()
{
    Tree tree(5);
    tree.insertChild(2, 5);
    tree.insertChild(6, 5);
    tree.insertChild(9, 5);
    tree.insertChild(4, 6);
    tree.print();
}