#include <stdio.h>
#include "list.h"

class Queue
{

private:
    class LinkedList ll;

public:
    // enqueue
    void enqueue(int x)
    {
        ll.addBack(x);
    }
    // dequeue
    void dequeue()
    {
        ll.removeFront();
    }
    // front
    int getFront()
    {
        return ll.get(0);
    }

    // isEmpty
    bool isEmpty()
    {
        return ll.isEmpty();
    }

    // print
    void print()
    {
        ll.print();
    }
};

int main()
{

    class Queue que;

    que.enqueue(56);
    que.enqueue(78);
    que.enqueue(75);
    que.print();
    que.dequeue();
    que.dequeue();
    que.print();
}