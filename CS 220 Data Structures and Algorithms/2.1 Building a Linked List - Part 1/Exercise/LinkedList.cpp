class ListNode
{

private:
    int data;
    ListNode *next;

public:
    ListNode();
    ListNode(int key);
    ListNode(int key, ListNode *node);
    void printNode(ListNode *node);

    friend class LinkedList;
};

ListNode::ListNode()
{
    next = NULL;
}

ListNode::ListNode(int key)
{
    data = key;
    next = NULL;
}

ListNode::ListNode(int key, ListNode *node)
{
    data = key;
    next = node;
}

void ListNode::printNode(ListNode *node)
{
    if (node == NULL)
        printf("\nEmpty Node!");
    printf("\nData: %d", node->data);
    printf("\nNext: %d", node->next->data);
}

class LinkedList
{
private:
    ListNode *head;
    ListNode *tail;

public:
    LinkedList();
    LinkedList(int headData);
    void printHead();
    void printTail();
    void print();

    void addFront(int x);
    void addBack(int x);

    void removeFront();
    void removeBack();
};

LinkedList::LinkedList()
{
    head = NULL;
    tail = head;
}

LinkedList::LinkedList(int headData)
{
    head = new ListNode(headData);
    tail = head;
}

void LinkedList::printHead()
{
    if (head == NULL)
        printf("\nHead is Null");
    printf("\nHead: %d", head->data);
}

void LinkedList::printTail()
{
    if (tail == NULL)
        printf("\nTail is Null");
    printf("\nTail: %d", tail->data);
}

void LinkedList::print()
{
    ListNode *iterator = head;
    printf("\n");
    while (iterator != NULL)
    {
        printf("%d ", iterator->data);
        iterator = iterator->next;
    }
}

void LinkedList::addFront(int x)
{
    if (head == NULL)
    {
        head->data = x;
        tail = head;
    }

    ListNode *temp = head;
    head = new ListNode(x);
    head->next = temp;
}

void LinkedList::addBack(int x)
{
    if (head == NULL)
    {
        tail->data = x;
        head = tail;
    }

    ListNode *temp = tail;
    tail = new ListNode(x);
    temp->next = tail;
}

void LinkedList::removeFront()
{
    if (head == NULL)
    {
        printf("\n Error! List is Empty");
    }

    ListNode *temp = head;
    head = temp->next;
    delete temp;
}

void LinkedList::removeBack()
{
    if (head == NULL)
    {
        printf("\n Error! List is Empty");
    }

    ListNode *iterator = head;
    while (iterator->next->next != NULL)
    {
        iterator = iterator->next;
    }

    delete tail;
    tail = iterator;
    tail->next = NULL;
}