#include <stdio.h>
#include "LinkedList.cpp"

int main()
{
    LinkedList list(20);
    list.addBack(30);
    list.addBack(40);
    list.addBack(50);
    list.addBack(60);
    list.print();
    list.removeBack();
    list.print();
    return 0;
}