#include <stdio.h>
#include <iostream>
using namespace std;
class Node
{
private:
    int data;
    class Node *next;

public:
    Node();
    Node(int x, class Node *nxt);
    friend class LinkedList;
};

Node::Node()
{
    next = NULL;
}

Node::Node(int x, class Node *nxt)
{
    data = x;
    next = nxt;
}

class LinkedList
{
private:
    class Node *head;
    class Node *tail;
    int length;

public:
    LinkedList();
    ~LinkedList();
    void addFront(int x);
    void removeFront();
    int find(int x);
    void print();
    int len();
    int get(int index);
};

LinkedList::LinkedList()
{
    head = NULL;
    tail = NULL;
    length = 0;
}

LinkedList::~LinkedList()
{
    while (head != NULL)
        removeFront();
}
/**
 *
 * This function adds an element to the front of the linkedList
 * 1) The head should be stored in a temp pointer
 * 2) The head data and the next pointer should be updated with the new one.
 * 3) The next for the new head will be the previous head i.e. temp in our case
 * 4) Increment the length property of LinkedList
 */
void LinkedList::addFront(int x)
{
    head = new Node(x, head);
    length++;
}

void LinkedList::removeFront()
{
    if (head == NULL)
        return;
    class Node *temp = head;
    head = head->next;
    delete temp;
    length--;
}

void LinkedList::print()
{
    class Node *iter = head;
    while (iter != NULL)
    {
        printf("%d ", iter->data);
        iter = iter->next;
    }
}

int LinkedList::find(int x)
{
    class Node *iter = head;
    while (iter != NULL)
    {
        if (iter->data == x)
            return 1;
        iter = iter->next;
    }
    return 0;
}

int LinkedList::get(int index)
{
    class Node *iter = head;
    while (iter != NULL)
    {
        if (index == 0)
            return iter->data;
        iter = iter->next;
        index--;
    }
    return -1;
}

int LinkedList::len()
{
    return length;
}

int main()
{

    class LinkedList ll;

    ll.addFront(1);
    ll.addFront(2);
    ll.addFront(3);
    ll.print();
    printf("\nLength is : %d", ll.len());
    // ll.removeFront();
    // printf("\n");
    // ll.print();
    // printf("\nLength is : %d", ll.len());
    // int x;
    // cout << "\nEnter element to search: ";
    // cin >> x;
    // printf("\nElement found: %d", ll.find(x));

    printf("\n Element at index 10 is: %d", ll.get(10));
}
