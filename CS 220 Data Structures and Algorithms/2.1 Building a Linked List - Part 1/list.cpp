/*
1. This the CPP class style implementaion of LinkedList
*/
#include <stdio.h>
using namespace std;
class Node
{
private:
    int elem;
    class Node *next;

public:
    // This constructor is used to add the element on the specified next pointer
    Node(int x, class Node *nxt)
    {
        elem = x;
        next = nxt;
    };

    // This constructor is used to add the element and set next as NULL
    Node(int x)
    {
        elem = x;
        next = NULL;
    }

    // This constructor is used to create an empty Node in LL
    Node()
    {
        next = NULL;
    }
    friend class LinkedList;
};

class LinkedList
{
private:
    class Node *head;
    int length;

public:
    LinkedList();
    ~LinkedList();
    void addFront(int x);
    void removeFront();
    int len();
    void print();
    // int search(int x);
};

LinkedList::LinkedList()
{
    length = 0;
    head = NULL;
}

LinkedList::~LinkedList()
{
    while (head != NULL)
    {
        class Node *iter = head; // important step other wise once the head pointer is deleted we lose access to the memory and it remains stagnant
        head = iter->next;
        delete iter;
    }
}

void LinkedList::addFront(int x)
{
    class Node *temp = head;
    head = new Node();
    head->elem = x;
    head->next = temp;
    length++;
}

void LinkedList::print()
{
    class Node *iter = head;
    while (iter != NULL)
    {
        printf("%d ", iter->elem);
        iter = iter->next;
    }
}

void LinkedList::removeFront()
{
    if (head == NULL)
        return;
    class Node *temp = head;
    head = head->next;
    delete temp;
    length--;
}

int LinkedList::len()
{
    return length;
}

int main()
{
    printf("Class compiled successfully.\n");
    class LinkedList ll;
    ll.addFront(5);
    ll.addFront(6);
    ll.addFront(7);
    ll.print();
    // printf("\nLength is %d ", ll.len());
}