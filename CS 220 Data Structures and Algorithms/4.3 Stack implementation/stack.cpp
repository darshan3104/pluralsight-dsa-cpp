#include <stdio.h>
#include "list.h"

class Stack
{
private:
    class LinkedList ll;

public:
    void push(int x)
    {
        ll.addFront(x);
    };

    void pop()
    {
        ll.removeFront();
    };

    bool isEmpty()
    {
        return ll.isEmpty();
    }

    void print()
    {
        ll.print();
    };
};

int main()
{
    class Stack stk;

    stk.push(7);
    stk.push(72);
    stk.push(56);
    stk.push(5);
    stk.print();

    stk.pop();
    stk.print();
}