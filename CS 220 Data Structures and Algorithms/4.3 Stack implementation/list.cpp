#include <stdio.h>
#include "list.h";

int main()
{

    class LinkedList ll;
    ll.addFront(7);
    ll.addFront(4);
    ll.addFront(9);
    ll.print();
    ll.removeFront();
    ll.print();
    ll.removeFront();
    ll.print();
}