#include <stdio.h>

class BTreeNode
{
private:
    int key;
    class BTreeNode *left;
    class BTreeNode *right;

public:
    BTreeNode(int key);
    friend class BTree;
    // Destructor
    ~BTreeNode()
    {
        // Recursive deletion of left and right subtrees
        delete left;
        delete right;
    }
};

BTreeNode::BTreeNode(int x)
{
    key = x;
    left = NULL;
    right = NULL;
}

class BTree
{
private:
    class BTreeNode *root;
    void insertRecursive(BTreeNode *rootNode, int key);
    void preOrder(BTreeNode *node);
    void postOrder(BTreeNode *node);
    void inOrder(BTreeNode *node);
    bool search(BTreeNode *node, int key);

public:
    int numOfNodes;
    BTree();
    // Destructor
    ~BTree()
    {
        // Delete the entire tree by deleting the root
        delete root;
    }
    void insert(int key);
    void searchElement(int x);
    void traversePreOrder();
    void traversePostOrder();
    void traverseInOrder();
};

BTree::BTree()
{
    root = NULL;
    numOfNodes = 0;
}

// Private insert
void BTree::insertRecursive(BTreeNode *rootNode, int key)
{
    // check if the root node is avaible if not then set
    if (root == NULL)
    {
        root = new BTreeNode(key);
        numOfNodes++;
        return;
    }

    if (rootNode == NULL)
    {
        rootNode = new BTreeNode(key);
        numOfNodes++;
        return;
    }

    // compare with the left value
    if (rootNode->key < key)
    {
        if (rootNode->right == NULL)
        {
            rootNode->right = new BTreeNode(key);
            numOfNodes++;
            return;
        }
        else
        {
            insertRecursive(rootNode->right, key);
        }
    }
    else if (rootNode->key > key)
    {
        if (rootNode->left == NULL)
        {
            rootNode->left = new BTreeNode(key);
            numOfNodes++;
            return;
        }
        else
        {
            insertRecursive(rootNode->left, key);
        }
    }
}

// public insert
void BTree::insert(int key)
{
    insertRecursive(root, key);
}

void BTree::preOrder(BTreeNode *node)
{
    if (node == NULL)
        return;
    printf("%d ", node->key);
    preOrder(node->left);
    preOrder(node->right);
}

void BTree::postOrder(BTreeNode *node)
{
    if (node == NULL)
        return;
    postOrder(node->left);
    postOrder(node->right);
    printf("%d ", node->key);
}

void BTree::inOrder(BTreeNode *node)
{
    if (node == NULL)
        return;
    inOrder(node->left);
    printf("%d ", node->key);
    inOrder(node->right);
}

void BTree::traversePreOrder()
{
    preOrder(root);
}

void BTree::traversePostOrder()
{
    postOrder(root);
}

void BTree::traverseInOrder()
{
    inOrder(root);
}

bool BTree::search(BTreeNode *node, int key)
{
    if (node == NULL)
        return false;
    if (node->key == key)
        return true;
    else if (node->key < key)
        return search(node->right, key);
    else if (node->key > key)
        return search(node->left, key);
}

void BTree::searchElement(int key)
{
    bool result = search(root, key);
    if (result)
    {
        printf("\nElement found!");
    }
    else
    {
        printf("\nElement not found!");
    }
}

int main()
{
    class BTree tree;
    tree.insert(70);
    tree.insert(50);
    tree.insert(110);
    tree.insert(30);
    tree.insert(60);
    tree.insert(80);
    tree.insert(150);
    tree.insert(10);
    tree.insert(90);
    printf("\nNumber of nodes present: %d", tree.numOfNodes);
    printf("\nPreOrder traversal:");
    tree.traversePreOrder();
    printf("\nPostOrder traversal:");
    tree.traversePostOrder();
    printf("\nInOrder traversal:");
    tree.traverseInOrder();
    tree.searchElement(100);
    return 0;
}
