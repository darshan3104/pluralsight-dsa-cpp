#include <stdio.h>

class BTreeNode
{

private:
    int key;
    BTreeNode *left;
    BTreeNode *right;

public:
    BTreeNode(int x);

    friend class BTree;
};

BTreeNode::BTreeNode(int x)
{
    key = x;
    left = NULL;
    right = NULL;
}

class BTree
{

private:
    BTreeNode *root;
    void insertNodeRecursive(BTreeNode* node, int data);
    void preOrder(BTreeNode* rootNode);
    void postOrder(BTreeNode* rootNode);

public:
    BTree(int r);
    void insertNode(int data);
    void printPreOrder();
    void printPostOrder();
};

BTree::BTree(int r)
{
    root = new BTreeNode(r);
}

void BTree::insertNodeRecursive(BTreeNode* node, int data){
    // Base case
    if(node == NULL) {
        node = new BTreeNode(data);
        return;
    }

    // recursive case
    if(node->key > data){
        if(node->left == NULL){
            node->left = new BTreeNode(data);
            return;
        }else{
            insertNodeRecursive(node->left, data);
        }
    }else if(node->key < data){
        if(node->right == NULL){
            node->right = new BTreeNode(data);
            return;
        }else{
            insertNodeRecursive(node->right, data);
        }
    }
}

void BTree::insertNode(int data){
    insertNodeRecursive(root,data);
}

void BTree::preOrder(BTreeNode* rootNode){
    if(rootNode == NULL) return;
    printf("%d ", rootNode->key);
    preOrder(rootNode->left);
    preOrder(rootNode->right);
}

void BTree::printPreOrder(){
    preOrder(root);
}

void BTree::postOrder(BTreeNode* rootNode){
    if(rootNode == NULL) return;
    postOrder(rootNode->left);
    postOrder(rootNode->right);
    printf("%d ", rootNode->key);
}

void BTree::printPostOrder(){
    postOrder(root);
}


int main()
{
    BTree tree(70);
    tree.insertNode(50);
    tree.insertNode(60);
    tree.insertNode(90);
    tree.insertNode(40);
    printf("\nPreOrder traversal of the tree is:");
    tree.printPreOrder();
    printf("\nPostOrder traversal of the tree is:");
    tree.printPostOrder();
    return 0;
}