#include <stdio.h>
#include <iostream>

class Node
{
private:
    class Node *prev;
    class Node *next;
    int data;

public:
    Node(){
        prev = NULL;
        next = NULL;
        data = 0;
    };
    Node(int x, class Node *prv, class Node *nxt){
        prev = prv;
        next = nxt;
        data = x;
    };
    

friend class DLL;
};

class DLL
{
public:
    class Node *head;
    class Node *tail;
    int length;

public:
    DLL(){
        head = NULL;
        tail = NULL;
        length = 0;
    };

    ~DLL(){
        class Node* iter = head;
        while(iter != NULL){
            class Node*temp = iter;
            iter = temp->next;
            delete temp;
        }
        delete head;
    }

    void getHead(){
        printf("\nHead is at %d", head->data);
    }

    void getTail(){
        printf("\nTail is at %d", tail->data);
    }

    void addFront(int x){
        if(head == NULL){
            head = new Node(x, NULL, NULL);
            tail = head;
        }else{
            head = new Node(x, NULL, head);
            head->next->prev = head->next;
            /**
             * The above line is equivalent to 
             * 1) store current head in a temp pointer: class Node* temp = head;
             * 2) Then assign the temp->prev pointer with the new Head next pointer
            */
        }
        length++;
    };

    void addBack(int x){
        if(tail == NULL){
            tail = new Node(x, NULL, NULL);
            head = tail;
        }else{
            tail = new Node(x,tail,NULL);
            tail->prev->next = tail;
        }
        length++;
    }

    void removeFront(){
        if(head == NULL) return;
        head = head->next;
        head->next->prev = NULL;
        length--;
    }

    void print(){
        class Node* iter = head;
        while(iter != NULL){
            printf("%d ", iter->data);
            iter = iter->next;
        }
    }

    void len(){
        printf("\nLength of DLL is: %d",length);
    }
};


int main(){

    class DLL dll;
    dll.addFront(5);
    dll.addFront(6);
    dll.addFront(7);
    // dll.getTail();
    // dll.addBack(8);
    dll.print();
    dll.removeFront();
    dll.print();
    dll.len();
    dll.getHead();
    dll.getTail();
}
